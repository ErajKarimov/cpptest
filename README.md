## CPPTEST
A simple C++ test project.
## Requirements

CMake version 3.25 or higher

A compiler that supports the C++20 standard

## Build and Run

Clone the repository:
```bash
git clone https://gitlab.com/ErajKarimov/cpptest.git
```

In the root of the project, create a build directory:
```bash
mkdir build && cd build
cmake ..
make
```
Run the executable file:
```bash
./cpptest
./cpp_gtest
```

## Project Structure

**StringBuilder.cpp** and **StringBuilder.hpp** - Implementation and declaration of the StringBuilder class for string handling.

**FileData.cpp** and **FileData.hpp** - Implementation and declaration of the FileData class for text data handling.

**tests/test.cpp** - Implementation of GoogleTest

**CMakeLists.txt** - Configuration file for CMake.

**.gitlab-ci.yml** - Configuration file for GitLab CI/CD.

## Performance Benchmarks

| Test Case | Old Time (s) | New Time (s) | Improvement Factor |
|-----------|--------------|--------------|--------------------|
| 10,000 strings  | 2.12575       | 0.00511559    | ~416x              |
| 20,000 strings  | 14.2501       | 0.00832787    | ~1711x             |
| 30,000 strings  | 37.7444       | 0.0120546     | ~3131x             |
| 40,000 strings  | 136.776       | 0.0157628     | ~8677x             |
