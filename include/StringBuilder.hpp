#ifndef STRING_BUILDER_HPP
#define STRING_BUILDER_HPP

#include <random>
#include <string>
#include <vector>


class StringBuilder {
public:
    void GenerateTestData(size_t stringCount);

    void GenerateTestDataOld(size_t stringCount);

    [[nodiscard]] std::string GetData() const;

    [[nodiscard]] std::string GetDataOld() const;

private:
    std::vector<std::string> strings;
    size_t totalLength = 0;
};

#endif // STRING_BUILDER_HPP
