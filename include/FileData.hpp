#ifndef FILE_DATA_HPP
#define FILE_DATA_HPP

#include <string>

class FileData {
public:
    FileData();

    FileData(const FileData &other) = default;

    FileData &operator=(const FileData &other); // Copy assignment operator
    ~FileData() = default;

    [[nodiscard]] const std::string& getText() const;

private:
    std::string myText;
};

#endif // FILE_DATA_HPP
