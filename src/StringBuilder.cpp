#include <StringBuilder.hpp>

void StringBuilder::GenerateTestData(size_t stringCount) {
    strings.clear();
    strings.reserve(stringCount);

    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_int_distribution<size_t> dist(1, 2000);

    for (size_t i = 0, strLen = 0; i < stringCount; ++i) {
        strLen = dist(gen);
        totalLength += strLen;
        strings.emplace_back(strLen, 'a');
    }
}

std::string StringBuilder::GetData() const {
    std::string result;
    result.reserve(totalLength);

    for (const auto &str: strings) {
        result += str;
    }

    return result;
}


void StringBuilder::GenerateTestDataOld(size_t nStrCount) {
    strings.clear();
    strings.reserve(nStrCount);

    // generate random string length
    std::random_device rd;
    std::uniform_int_distribution<size_t> rnd_generator(1, 2000);

    std::string s;
    // generate nStrCount test strings
    for (size_t i = 0; i < nStrCount; ++i) {
        size_t nStrLength = rnd_generator(rd);
        s.erase();
        s.append(nStrLength, 'a');
        strings.push_back(s);
    }
}

////////////////////////////////////////////////////////////////////////////////
// returns string concatenated from m_vStrings
std::string StringBuilder::GetDataOld() const {
    std::string s;
    for (auto str: strings) {
        // printf("Adding string: " + str);
        s = s + str;
    }
    return s;
}

