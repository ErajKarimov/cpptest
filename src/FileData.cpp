#include <FileData.hpp>

FileData::FileData() : myText("Initial Text") {}

// FileData::FileData(const FileData& other) : myText(other.myText) {}

FileData &FileData::operator=(const FileData &other) {
    if (this != &other) {
        myText = other.myText;
    }
    return *this;
}

const std::string& FileData::getText() const { return myText; }
