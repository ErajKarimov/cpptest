#include <chrono>
#include <gtest/gtest.h>
#include <FileData.hpp>
#include <StringBuilder.hpp>


class StringBuilderTest : public ::testing::Test {
protected:
    StringBuilder stringBuilder;

    std::chrono::duration<double>
    MeasureMethodPerformance(void (StringBuilder::*method_gtd)(size_t), std::string (StringBuilder::*method_gt)() const,
                             size_t arg) {
        (stringBuilder.*method_gtd)(arg);
        auto startTime = std::chrono::high_resolution_clock::now();
        std::cout << "String_size = " << (stringBuilder.*method_gt)().size() << std::endl;
        auto endTime = std::chrono::high_resolution_clock::now();
        return endTime - startTime;
    }
};

// Тест для проверки производительности после ревью
TEST_F(StringBuilderTest, TestPerfNew) {
    size_t strCount = 10000;
    auto duration = MeasureMethodPerformance(&StringBuilder::GenerateTestData, &StringBuilder::GetData, strCount);
    std::cout << "GenerateTestData for " << strCount << " completed in " << duration.count() << " seconds\n";
    duration = MeasureMethodPerformance(&StringBuilder::GenerateTestData, &StringBuilder::GetData, strCount += 10000);
    std::cout << "GenerateTestData for " << strCount << " completed in " << duration.count() << " seconds\n";
    duration = MeasureMethodPerformance(&StringBuilder::GenerateTestData, &StringBuilder::GetData, strCount += 10000);
    std::cout << "GenerateTestData for " << strCount << " completed in " << duration.count() << " seconds\n";
    duration = MeasureMethodPerformance(&StringBuilder::GenerateTestData, &StringBuilder::GetData, strCount += 10000);
    std::cout << "GenerateTestData for " << strCount << " completed in " << duration.count() << " seconds\n";
}

// Тест для проверки производительности до ревью
TEST_F(StringBuilderTest, TestPerfOld) {
    size_t strCount = 10000;
    auto duration = MeasureMethodPerformance(&StringBuilder::GenerateTestDataOld, &StringBuilder::GetDataOld, strCount);
    std::cout << "GenerateTestData for " << strCount << " completed in " << duration.count() << " seconds\n";
    duration = MeasureMethodPerformance(&StringBuilder::GenerateTestDataOld, &StringBuilder::GetDataOld,
                                        strCount += 10000);
    std::cout << "GenerateTestData for " << strCount << " completed in " << duration.count() << " seconds\n";
    duration = MeasureMethodPerformance(&StringBuilder::GenerateTestDataOld, &StringBuilder::GetDataOld,
                                        strCount += 10000);
    std::cout << "GenerateTestData for " << strCount << " completed in " << duration.count() << " seconds\n";
    duration = MeasureMethodPerformance(&StringBuilder::GenerateTestDataOld, &StringBuilder::GetDataOld,
                                        strCount += 10000);
    std::cout << "GenerateTestData for " << strCount << " completed in " << duration.count() << " seconds\n";
}


// Тест для проверки конструктора
TEST(FileDataTest, Constructor) {
    FileData data;
    EXPECT_EQ(data.getText(), "Initial Text") << "Constructor should initialize myText with 'Initial Text'";
}

// Тест для проверки копирующего конструктора
TEST(FileDataTest, CopyConstructor) {
    FileData original;
    FileData copy(original);
    EXPECT_EQ(copy.getText(), original.getText()) << "Copy constructor should copy myText correctly";
}

// Тест для проверки оператора присваивания
TEST(FileDataTest, CopyAssignment) {
    FileData original;
    FileData copy;
    copy = original;
    EXPECT_EQ(copy.getText(), original.getText()) << "Copy assignment operator should copy myText correctly";
}


int main(int argc, char **argv) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
